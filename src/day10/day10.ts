import * as fs from "fs";

export class Day10 {
  run(): void {
    console.log('Day 10');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const jolts = this._getJolts();
    const differences = this._getDifferences(jolts);
    const answer = this._multiply(differences);

    console.log('Solution 1', answer);
  }

  private _solution2(): void {
    const adapterRatings = this._getJolts();
    const onesInSequence = this._getAmountOfOnesInSequence(adapterRatings);
    const answer = this._getAmountOfArrangements(onesInSequence);

    console.log('Solution 2', answer);
  }

  private _getJolts(): number[] {
    const adapterRatings = fs.readFileSync('src/day10/input.txt', 'utf-8')
      .split('\n')
      .map(line => parseInt(line, 10))
      .sort((a: number, b: number) => a - b);
    
    const outletJolt = 0;
    const deviceJolt = adapterRatings[adapterRatings.length - 1] + 3;

    return [outletJolt, ...adapterRatings, deviceJolt];
  }

  private _getDifferences(jolts: number[]): number[] {
    let ones = 0;
    let threes = 0;

    for (let i = 1; i < jolts.length; i++) {
      const previousJolt = jolts[i - 1];
      const differenceToPrevious = jolts[i] - previousJolt;

      switch (differenceToPrevious) {
        case 1:
          ones++;
          break;
        case 3:
          threes++;
          break;
      }
    }

    return [ones, threes];
  }

  private _multiply(numbers: number[]): number {
    return numbers.reduce((acc, curr) => acc * curr);
  }

  private _getAmountOfOnesInSequence(jolts: number[]): number[] {
    const onesInSequence: number[] = [];
    let currentSequence = 1;

    for (let i = 0; i < jolts.length; i++) {
      const previousJolt = jolts[i - 1];

      if (jolts[i] - previousJolt === 1) {
        currentSequence++;
      } else if (currentSequence > 1) {
        if (currentSequence >= 3) {
          onesInSequence.push(currentSequence);
        }

        currentSequence = 1;
      }
    }

    return onesInSequence;
  }

  private _getAmountOfArrangements(numbers: number[]): number {
    return numbers.reduce((acc, curr) => {
      switch (curr) {
        case 3:
          return acc * 2;
        case 4:
          return acc * 4;
        case 5:
          return acc * 7;
        default:
          return acc;
      }
    }, 1)
  }
}
