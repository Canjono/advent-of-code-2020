import * as fs from "fs";

class Instruction {
  operation: string;
  argument: number;

  constructor(operation: string, argument: number) {
    this.operation = operation;
    this.argument = argument;
  }
}

export class Day8 {
  run(): void {
    console.log('Day 8');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const instructions = this._getInstructions();
    const accumulator = this._getAccumulatorBeforeSecondLoop(instructions);

    console.log('Solution 1', accumulator);
  }

  private _solution2(): void {
    const instructions = this._getInstructions();
    const accumulator = this._getAccumulatorAfterProgramTermination(instructions);

    console.log('Solution 2', accumulator);
  }

  private _getInstructions(): Instruction[] {
    return fs.readFileSync('src/day08/input.txt', 'utf-8')
      .split('\n')
      .map(line => {
        const content = line.split(' ');
        return new Instruction(content[0], parseInt(content[1], 10));
      });
  }

  private _getAccumulatorBeforeSecondLoop(instructions: Instruction[]): number {
    let accumulator = 0;
    let index = 0;
    const traversedIndices: number[] = [];

    while (!traversedIndices.includes(index)) {
      traversedIndices.push(index);
      const instruction = instructions[index];

      switch (instruction.operation) {
        case 'acc':
          accumulator += instruction.argument;
          index++;
          break;
        case 'jmp':
          index += instruction.argument;
          break;
        case 'nop':
          index++;
          break;
      }
    }

    return accumulator;
  }

  private _getAccumulatorAfterProgramTermination(instructions: Instruction[]): number {
    let accumulator = 0;
    let index = 0;
    let foundBuggyInstruction = false;

    while (index < instructions.length) {
      const instruction = instructions[index];

      if (!foundBuggyInstruction && instruction.operation !== 'acc') {
        if (this._tryIfLeadsToTermination(instructions, index)) {
          foundBuggyInstruction = true;
        } else {
          instruction.operation = instruction.operation === 'jmp' ? 'nop' : 'jmp';
        }
      }

      switch (instruction.operation) {
        case 'acc':
          accumulator += instruction.argument;
          index++;
          break;
        case 'jmp':
          index += instruction.argument;
          break;
        case 'nop':
          index++;
          break;
      }
    }

    return accumulator;
  }

  private _tryIfLeadsToTermination(instructions: Instruction[], index: number): boolean {
    const testInstruction = instructions[index];

    if (testInstruction.operation === 'acc') {
      return false;
    }

    testInstruction.operation = testInstruction.operation === 'jmp' ? 'nop' : 'jmp';
    const traversedIndices: number[] = [];

    while (index < instructions.length && !traversedIndices.includes(index)) {
      traversedIndices.push(index);
      const instruction = instructions[index];

      switch (instruction.operation) {
        case 'acc':
          index++;
          break;
        case 'jmp':
          index += instruction.argument;
          break;
        case 'nop':
          index++;
          break;
      }
    }

    return index === instructions.length;
  }
}
