import * as fs from "fs";

class Group {
  yesAnswers: string[];
  totalYesFromSome: number;
  totalYesFromAll: number;
  totalGroupMembers: number;

  constructor(yesAnswers: string[]) {
    this.yesAnswers = yesAnswers;
    this.totalGroupMembers = yesAnswers.length;
  }
}

export class Day6 {
  run(): void {
    console.log('Day 6');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const groups = this._getGroups();
    this._calculateTotalYesFromSome(groups);
    const sum = this._addAllYesFromSome(groups);

    console.log('Solution 1', sum);
  }

  private _solution2(): void {
    const groups = this._getGroups();
    this._calculateTotalYesFromAll(groups);
    const sum = this._addAllYesFromAll(groups);

    console.log('Solution 2', sum);
  }

  private _getGroups(): Group[] {
    return fs.readFileSync('src/day06/input.txt', 'utf-8')
      .split('\n\n')
      .map(groupAnswers => new Group(groupAnswers.split('\n')));
  }

  private _calculateTotalYesFromSome(groups: Group[]): void {
    for (const group of groups) {
      const foundYes = new Map();
      for (const yesAnswers of group.yesAnswers) {
        for (const question of yesAnswers) {
          foundYes.set(question, true);
        }
      }
      group.totalYesFromSome = foundYes.size;
    }
  }

  private _addAllYesFromSome(groups: Group[]): number {
    let sum = 0;

    for (const group of groups) {
      sum += group.totalYesFromSome;
    }

    return sum;
  }

  private _calculateTotalYesFromAll(groups: Group[]): void {
    for (const group of groups) {
      const foundYes = new Map();
      for (const yesAnswers of group.yesAnswers) {
        for (const question of yesAnswers) {
          if (foundYes.has(question)) {
            let amount = foundYes.get(question);
            foundYes.set(question, ++amount);
          } else {
            foundYes.set(question, 1);
          }
        }
      }

      group.totalYesFromAll = 0;

      foundYes.forEach((amount: number) => {
        if (amount === group.totalGroupMembers) {
          group.totalYesFromAll++;
        }
      });
    }
  }

  private _addAllYesFromAll(groups: Group[]): number {
    let sum = 0;

    for (const group of groups) {
      sum += group.totalYesFromAll;
    }

    return sum;
  }
}