import * as fs from "fs";

enum SeatType {
  Floor = '.',
  Empty = 'L',
  Occupied = '#'
}

class SeatLayout {
  width: number;
  height: number;
  seats: string[][];

  constructor(seats: string[][]) {
    this.seats = seats.map(seat => Array.from(seat));
    this.width = seats[0].length;
    this.height = seats.length;
  }

  getAdjacentSeats(y: number, x: number): string[] {
    const adjacentSeats: string[] = [];

    // Seats above.
    if (y > 0) {
      adjacentSeats.push(this.seats[y - 1][x]); // Above.
      if (x > 0) {
        adjacentSeats.push(this.seats[y - 1][x - 1]); // Above left.
      }
      if (x < this.width - 1) {
        adjacentSeats.push(this.seats[y - 1][x + 1]) // Above right.
      }
    }
    // Seats below.
    if (y < this.height - 1) {
      adjacentSeats.push(this.seats[y + 1][x]); // Below.
      if (x > 0) {
        adjacentSeats.push(this.seats[y + 1][x - 1]); // Below left.
      }
      if (x < this.width - 1) {
        adjacentSeats.push(this.seats[y + 1][x + 1]); // Below right.
      }
    }
    // Seats on sides.
    if (x > 0) {
      adjacentSeats.push(this.seats[y][x - 1]); // Left.
    }
    if (x < this.width - 1) {
      adjacentSeats.push(this.seats[y][x + 1]); // Right.
    }

    return adjacentSeats;
  }

  getFirstSeenOccupiedSeats(y: number, x: number): number {
    let occupiedSeats = 0;
    const directions = [
      [-1, 0],
      [-1, -1],
      [-1, 1],
      [0, -1],
      [0, 1],
      [1, 0],
      [1, -1],
      [1, 1]
    ];

    for (const direction of directions) {
      if (this._foundOccupiedInDirection(y, x, direction[0], direction[1])) {
        occupiedSeats++;
      }
    }

    return occupiedSeats;
  }

  private _foundOccupiedInDirection(y: number, x: number, yDir: number, xDir: number): boolean {
    const currentPos = {
      y: y,
      x: x
    };

    // eslint-disable-next-line no-constant-condition
    while (true) {
      currentPos.y = currentPos.y + yDir;
      currentPos.x = currentPos.x + xDir;
      const yIsWithinSeats = currentPos.y >= 0 && currentPos.y < this.height;
      const xIsWithinSeats = currentPos.x >= 0 && currentPos.x < this.width;

      if (!yIsWithinSeats || !xIsWithinSeats) {
        return false;
      }

      switch (this.seats[currentPos.y][currentPos.x]) {
        case SeatType.Occupied:
          return true;
        case SeatType.Empty:
          return false;
      }
    }
  }

  getAmountOfOccupiedSeats(): number {
    let counter = 0;

    for (const row of this.seats) {
      for (const seat of row) {
        if (seat === SeatType.Occupied) {
          counter++;
        }
      }
    }

    return counter;
  }
}

export class Day11 {
  run(): void {
    console.log('Day 11');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const seatLayout = this._getSeatLayout();
    const amountOfOccupiedSeats = this._getAmountOfOccupiedSeats(seatLayout);

    console.log('Solution 1:', amountOfOccupiedSeats);
  }

  private _solution2(): void {
    const seatLayout = this._getSeatLayout();
    const amountOfOccupiedSeats = this._getAmountOfOccupiedSeats2(seatLayout);

    console.log('Solution 2:', amountOfOccupiedSeats);
  }

  private _getSeatLayout(): SeatLayout {
    const lines = fs.readFileSync('src/day11/input.txt', 'utf-8')
      .split('\n');

    const seats: string[][] = [];

    for (let i = 0; i < lines.length; i++) {
      seats.push([]);
      for (let j = 0; j < lines[i].length; j++) {
        seats[i].push(lines[i][j]);
      }
    }

    return new SeatLayout(seats);
  }

  private _getAmountOfOccupiedSeats(seatLayout: SeatLayout): number {
    let wasStateChanges = true;
    let previousLayout = new SeatLayout(seatLayout.seats);
    let newLayout = new SeatLayout([[]]);

    while (wasStateChanges) {
      wasStateChanges = false;
      newLayout = new SeatLayout(previousLayout.seats);

      for (let y = 0; y < newLayout.height; y++) {
        for (let x = 0; x < newLayout.width; x++) {
          const currentSeat = newLayout.seats[y][x];

          if (currentSeat === SeatType.Floor) {
            continue;
          }

          const adjacentSeats = previousLayout.getAdjacentSeats(y, x);
          const adjacentOccupiedSeats = adjacentSeats.filter(seat => seat === SeatType.Occupied);

          if (currentSeat === SeatType.Occupied && adjacentOccupiedSeats.length >= 4) {
            newLayout.seats[y][x] = SeatType.Empty;
            wasStateChanges = true;
            continue;
          }

          if (currentSeat === SeatType.Empty && adjacentOccupiedSeats.length === 0) {
            newLayout.seats[y][x] = SeatType.Occupied;
            wasStateChanges = true;
          }
        }
      }

      previousLayout = new SeatLayout(newLayout.seats);
    }

    return newLayout.getAmountOfOccupiedSeats();
  }

  private _getAmountOfOccupiedSeats2(seatLayout: SeatLayout): number {
    let wasStateChanges = true;
    let previousLayout = new SeatLayout(seatLayout.seats);
    let newLayout = new SeatLayout([[]]);

    while (wasStateChanges) {
      wasStateChanges = false;
      newLayout = new SeatLayout(previousLayout.seats);

      for (let y = 0; y < newLayout.height; y++) {
        for (let x = 0; x < newLayout.width; x++) {
          const currentSeat = newLayout.seats[y][x];

          if (currentSeat === SeatType.Floor) {
            continue;
          }

          const occupiedSeats = previousLayout.getFirstSeenOccupiedSeats(y, x);

          if (currentSeat === SeatType.Occupied && occupiedSeats >= 5) {
            newLayout.seats[y][x] = SeatType.Empty;
            wasStateChanges = true;
            continue;
          }

          if (currentSeat === SeatType.Empty && occupiedSeats === 0) {
            newLayout.seats[y][x] = SeatType.Occupied;
            wasStateChanges = true;
          }
        }
      }

      previousLayout = new SeatLayout(newLayout.seats);
    }

    return newLayout.getAmountOfOccupiedSeats();
  }
}