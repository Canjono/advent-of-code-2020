import * as fs from "fs";

class Bag {
  color: string;
  content: Map<string, number>;

  constructor(color: string, content: string) {
    this.color = color;
    this._setContent(content);
  }

  private _setContent(content: string): void {
    this.content = new Map();

    if (content === 'no other bags') {
      return;
    }

    const regex = /^(\d+) (.*) bags?$/
    const bags = content.split(', ');

    for (const bag of bags) {
      const matches = bag.match(regex);

      if (!matches) {
        throw new Error(`Content bag ${bag} couldn't be parsed`);
      }

      this.content.set(matches[2], parseInt(matches[1], 10));
    }
  }
}

export class Day7 {
  private _shinyGold = 'shiny gold';

  run(): void {
    console.log('Day 7');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const bags = this._getBags();
    const amount = this._getAmountThatContainsShinyBag(bags);

    console.log('Solution 1:', amount);
  }

  private _solution2(): void {
    const bags = this._getBags();
    const amount = this._getAmountInShinyBag(bags);

    console.log('Solution 2:', amount);
  }

  private _getBags(): Bag[] {
    const regex = /^(.*) bags contain (.*)\.$/

    return fs.readFileSync('src/day07/input.txt', 'utf-8')
      .split('\n')
      .map(line => {
        const matches = line.match(regex);

        if (!matches) {
          throw new Error(`Line ${line} didn't match bag rule pattern`)
        }

        return new Bag(matches[1], matches[2]);
      });
  }

  private _getAmountThatContainsShinyBag(allBags: Bag[]): number {
    const shinyBagContainers = new Map();

    for (const bag of allBags) {
      if (bag.content.has(this._shinyGold)) {
        shinyBagContainers.set(bag.color, null);
        const parents = this._getParents(bag, allBags);

        for (const parent of parents) {
          shinyBagContainers.set(parent.color, null);
        }
      }
    }

    return shinyBagContainers.size;
  }

  private _getParents(currentBag: Bag, allBags: Bag[]): Bag[] {
    const parents: Bag[] = [];

    for (const bag of allBags) {
      if (bag.content.has(currentBag.color)) {
        parents.push(bag);
        this._getParents(bag, allBags)
          .forEach(bag => parents.push(bag));
      }
    }

    return parents;
  }

  private _getAmountInShinyBag(allBags: Bag[]): number {
    const shinyBag = allBags.find(bag => bag.color === this._shinyGold);

    if (!shinyBag) {
      return 0;
    }

    return this._getAmountOfChildren(shinyBag, allBags);
  }

  private _getAmountOfChildren(currentBag: Bag, allBags: Bag[]): number {
    let amount = 0;

    for (const child of currentBag.content) {
      amount += child[1];
      const childBag = allBags.find(rule => rule.color === child[0]);

      if (!childBag) {
        throw new Error(`Couldn't find childBag ${child[0]}`);
      }

      for (let i = 0; i < child[1]; i++) {
        amount += this._getAmountOfChildren(childBag, allBags);
      }
    }

    return amount;
  }
}
