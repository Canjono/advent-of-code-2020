import * as fs from 'fs';

export class Day1 {
  run(): void {
    console.log('Day 1');
    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const entries = this._getEntries();
    const entries2020 = this._get2020Entries(entries, false);
    const answer = this._multiply(entries2020);

    console.log(`Solution 1: ${answer}`);
  }

  private _solution2(): void {
    const entries = this._getEntries();
    const entries2020 = this._get2020Entries(entries, true);
    const answer = this._multiply(entries2020);

    console.log(`Solution 2: ${answer}`);
  }

  private _getEntries(): number[] {
    const entries = fs.readFileSync('src/day01/input.txt', 'utf-8')
        .split('\n')
        .map((entry: string) => parseInt(entry, 10));

    return entries;
  }

  private _get2020Entries(entries: number[], addThree: boolean): number[] {
    for (let i = 0; i < entries.length; i++) {
      for (let j = i + 1; j < entries.length; j++) {
        if (addThree && entries[i] + entries[j] < 2020) {
          for (let k = j + 1; k < entries.length; k++) {
            if (entries[i] + entries[j] + entries[k] === 2020) {
              return [entries[i], entries[j], entries[k]];
            }
          }
          continue;
        }
        if (entries[i] + entries[j] === 2020) {
          return [entries[i], entries[j]];
        }
      }
    }

    return [0];
  }

  private _multiply(entries: number[]): number {
    const product = entries.reduce((accumulator, current) =>
      accumulator * current, 1);

    return product;
  }
}
