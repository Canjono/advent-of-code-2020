import { Day1 } from './day01/day1';
import { Day10 } from './day10/day10';
import { Day11 } from './day11/day11';
import { Day2 } from './day02/day2';
import { Day3 } from './day03/day3';
import { Day4 } from './day04/day4';
import { Day5 } from './day05/day5';
import { Day6 } from './day06/day6';
import { Day7 } from './day07/day7';
import { Day8 } from './day08/day8';
import { Day9 } from './day09/day9';
import { Day12 } from './day12/day12';

new Day1().run();
new Day2().run();
new Day3().run();
new Day4().run();
new Day5().run();
new Day6().run();
new Day7().run();
new Day8().run();
new Day9().run();
new Day10().run();
new Day11().run();
new Day12().run();
