import * as fs from 'fs';

enum ValidationRule {
  Standard,
  Strict
}

class Passport {
  byr: number;
  iyr: number;
  eyr: number;
  hgt: string;
  hcl: string;
  ecl: string;
  pid: string;
  cid: string;

  constructor(data: string) {
    const fields = data.split(' ');

    for (const field of fields) {
      const keyValue = field.split(':');

      switch (keyValue[0]) {
        case 'byr':
          this.byr = parseInt(keyValue[1], 10);
          break;
        case 'iyr':
          this.iyr = parseInt(keyValue[1], 10);
          break;
        case 'eyr':
          this.eyr = parseInt(keyValue[1], 10);
          break;
        case 'hgt':
          this.hgt = keyValue[1];
          break;
        case 'hcl':
          this.hcl = keyValue[1];
          break;
        case 'ecl':
          this.ecl = keyValue[1];
          break;
        case 'pid':
          this.pid = keyValue[1];
          break;
        case 'cid':
          this.cid = keyValue[1];
          break;
      }
    }
  }
}

export class Day4 {
  run(): void {
    console.log('Day 4');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const passports = this._getPassports();
    const amountOfValidPassports = this._getAmountOfValidPassports(passports, ValidationRule.Standard);

    console.log('Solution 1:', amountOfValidPassports);
  }

  private _solution2(): void {
    const passports = this._getPassports();
    const amountOfValidPassports = this._getAmountOfValidPassports(passports, ValidationRule.Strict)

    console.log('Solution 2:', amountOfValidPassports);
  }

  private _getPassports(): Passport[] {
    const lines = fs.readFileSync('src/day04/input.txt', 'utf-8')
      .split('\n');

    const passports: Passport[] = [];
    let passportData = '';

    for (const line of lines) {
      if (line === '') {
        passports.push(new Passport(passportData));
        passportData = '';
        continue;
      }

      passportData += line + ' ';
    }

    passports.push(new Passport(passportData));

    return passports;
  }

  private _getAmountOfValidPassports(passports: Passport[], rule: ValidationRule): number {
    let amount = 0;

    switch (rule) {
      case ValidationRule.Standard:
        passports.forEach(passport => {
          const isValid = this._isValidStandardPassword(passport);
          if (isValid) {
            amount++;
          }
        });
        break;
      case ValidationRule.Strict:
        passports.forEach(passport => {
          const isValid = this._isValidStrictPassword(passport);
          if (isValid) {
            amount++;
          }
        });
        break;
    }
    
    return amount;
  }

  private _isValidStandardPassword(passport: Passport): boolean {
    return !!passport.byr && !!passport.ecl && !!passport.eyr && !!passport.hcl && !!passport.hgt && !!passport.iyr && !!passport.pid;

  }

  private _isValidStrictPassword(passport: Passport): boolean {
    const byrIsValid = passport.byr >= 1920 && passport.byr <= 2002;
    const iyrIsValid = passport.iyr >= 2010 && passport.iyr <= 2020;
    const eyrIsValid = passport.eyr >= 2020 && passport.eyr <= 2030;
    let hgtIsValid = false;

    if (passport.hgt) {
      const heightMatch = passport.hgt.match(/^(\d+)(cm|in)$/);
      if (heightMatch) {
        const number = parseInt(heightMatch[1], 10);
        switch (heightMatch[2]) {
          case 'in':
            hgtIsValid = number >= 59 && number <= 76;
            break;
          case 'cm':
            hgtIsValid = number >= 150 && number <= 193;
            break;
        }
      }
    }

    const hclIsValid = !!passport.hcl && !!passport.hcl.match(/^#[\da-f]{6}$/);
    const eclIsValid = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(passport.ecl);
    const pidIsValid = !!passport.pid && !!passport.pid.match(/^\d{9}$/);

    return byrIsValid && iyrIsValid && eyrIsValid && hgtIsValid && hclIsValid && eclIsValid && pidIsValid;
  }
}
