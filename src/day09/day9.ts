import * as fs from "fs";

export class Day9 {
  run(): void {
    console.log('Day 9');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const numbers = this._getNumbers();
    const firstInvalidNumber = this._getFirstInvalidNumber(numbers);

    console.log('Solution 1:', firstInvalidNumber);
  }

  private _solution2(): void {
    const numbers = this._getNumbers();
    const firstInvalidNumber = this._getFirstInvalidNumber(numbers);
    const contiguousNumbers = this._getContiguousNumbers(firstInvalidNumber, numbers);
    const answer = this._addMinAndMax(contiguousNumbers);

    console.log('Solution 2:', answer);
  }

  private _getNumbers(): number[] {
    return fs.readFileSync('src/day09/input.txt', 'utf-8')
      .split('\n')
      .map(line => parseInt(line, 10))
  }

  private _getFirstInvalidNumber(numbers: number[]): number {
    for (let i = 26; i < numbers.length; i++) {
      let foundValid = false;
      for (let j = i - 25; j < i - 1; j++) {
        for (let k = j + 1; k < i; k++) {
          if (numbers[j] + numbers[k] === numbers[i]) {
            foundValid = true;
            break;
          }
        }
        if (foundValid) {
          break;
        }
      }

      if (!foundValid) {
        return numbers[i];
      }
    }

    return -1;
  }

  private _getContiguousNumbers(invalidNumber: number, numbers: number[]): number[] {
    for (let i = 0; i < numbers.length; i++) {
      const contiguousNumbers = [numbers[i]];
      let total = numbers[i];

      for (let j = i + 1; j < numbers.length; j++) {
        total += numbers[j];
        contiguousNumbers.push(numbers[j]);

        if (total === invalidNumber) {
          return contiguousNumbers;
        }

        if (total > invalidNumber) {
          break;
        }
      }
    }

    return [];
  }

  private _addMinAndMax(numbers: number[]): number {
    const minValue = Math.min(...numbers);
    const maxValue = Math.max(...numbers);

    return minValue + maxValue;
  }
}
