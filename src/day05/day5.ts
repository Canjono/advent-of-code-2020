import * as fs from 'fs';

class BoardingPass {
  id: number;
  row: number;
  column: number;

  constructor(id: number, row: number, column: number) {
    this.id = id;
    this.row = row;
    this.column = column;
  }
}

export class Day5 {
  run(): void {
    console.log('Day 5');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const seatNavigations = this._getSeatNavigations();
    const boardingPasses = this._getBoardingPasses(seatNavigations)
    const highestId = this._getHighestId(boardingPasses);

    console.log('Solution 1', highestId);
  }

  private _solution2(): void {
    const seatNavigations = this._getSeatNavigations();
    const boardingPasses = this._getBoardingPasses(seatNavigations);
    const myId = this._findMissingSeatId(boardingPasses);

    console.log('Solution 2:', myId);
  }

  private _getSeatNavigations(): string[] {
    return fs.readFileSync('src/day05/input.txt', 'utf-8')
      .split('\n');
  }

  private _getBoardingPasses(seatNavigations: string[]): BoardingPass[] {
    const seatRows = this._getSeatRows();
    const seatColumns = this._getSeatColumns();

    return seatNavigations.map(navigation => {
      const seatRow = this._getRow(navigation, seatRows);
      const seatColumn = this._getColumn(navigation, seatColumns);
      const id = this._getId(seatRow, seatColumn);

      return new BoardingPass(id, seatRow, seatColumn);
    })
  }

  private _getSeatRows(): number[] {
    const seatRows: number[] = []

    for (let i = 0; i < 128; i++) {
      seatRows.push(i);
    }

    return seatRows;
  }

  private _getSeatColumns(): number[] {
    const seatColumns: number[] = [];

    for (let i = 0; i < 8; i++) {
      seatColumns.push(i);
    }

    return seatColumns;
  }

  private _getRow(seatNavigation: string, seatRows: number[]): number {
    if (seatRows.length === 1) {
      return seatRows[0];
    }

    const letter = seatNavigation[0];

    switch (letter) {
      case 'F':
        seatRows = seatRows.slice(0, seatRows.length / 2);
        break;
      case 'B':
        seatRows = seatRows.slice(seatRows.length / 2);
        break;
    }

    return this._getRow(seatNavigation.substring(1), seatRows);
  }

  private _getColumn(seatNavigation: string, seatColumns: number[]): number {
    if (seatColumns.length === 1) {
      return seatColumns[0];
    }

    const letter = seatNavigation[0];

    switch (letter) {
      case 'L':
        seatColumns = seatColumns.slice(0, seatColumns.length / 2);
        break;
      case 'R':
        seatColumns = seatColumns.slice(seatColumns.length / 2);
        break;
    }

    return this._getColumn(seatNavigation.substring(1), seatColumns);
  }

  private _getId(row: number, column: number): number {
    return row * 8 + column;
  }

  private _getHighestId(boardingPasses: BoardingPass[]): number {
    let highestId = 0;

    for (const boardingPass of boardingPasses) {
      if (boardingPass.id > highestId) {
        highestId = boardingPass.id;
      }
    }

    return highestId;
  }

  private _sortBoardingPasses(boardingPasses: BoardingPass[]): BoardingPass[] {
    return boardingPasses.sort((pass1: BoardingPass, pass2: BoardingPass) => {
      return pass1.id > pass2.id ? 1 : -1;
    });
  }

  private _findMissingSeatId(boardingPasses: BoardingPass[]): number {
    boardingPasses = this._sortBoardingPasses(boardingPasses);
    const minId = boardingPasses[0].id;
    const maxId = boardingPasses[boardingPasses.length - 1].id;

    for (let id = minId; id < maxId; id++) {
      const foundBoardingPass = boardingPasses.find(boardingPass => boardingPass.id === id);

      if (!foundBoardingPass) {
        return id;
      }
    }

    return -1;
  }
}
