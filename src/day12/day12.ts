import * as fs from "fs";

enum Direction {
  North,
  East,
  South,
  West
}

class Instruction {
  action: string;
  value: number;

  constructor(action: string, value: number) {
    this.action = action;
    this.value = value;
  }
}

class Position {
  horizontal: number;
  vertical: number;

  constructor (horizontal: number, vertical: number) {
    this.horizontal = horizontal;
    this.vertical = vertical;
  }
}

export class Day12 {
  run(): void {
    console.log('Day 12');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const instructions = this._getNaviationInstructions();
    const position = this._moveShip(instructions);
    const answer = this._addManhattanPosition(position.horizontal, position.vertical);

    console.log('Solution 1:', answer);
  }

  private _solution2(): void {
    const instructions = this._getNaviationInstructions();
    const position = this._moveShipToWaypoint(instructions);
    const answer = this._addManhattanPosition(position.horizontal, position.vertical);

    console.log('Solution 2:', answer);
  }

  private _getNaviationInstructions(): Instruction[] {
    const regex = /^(\w)(\d*)$/;
    return fs.readFileSync('src/day12/input.txt', 'utf-8')
      .split('\n')
      .map(line => {
        const matches = line.match(regex);

        if (!matches) { 
          throw new Error(`Line ${line} didn't match regex`);
        }

        return new Instruction(matches[1], parseInt(matches[2], 10));
      })
  }

  private _moveShip(instructions: Instruction[]): Position {
    const position = new Position(0, 0);

    let direction = Direction.East;

    for (const instruction of instructions) {
      switch (instruction.action) {
        case 'N':
          position.vertical -= instruction.value;
          break;
        case 'W':
          position.horizontal -= instruction.value;
          break;
        case 'S':
          position.vertical += instruction.value;
          break;
        case 'E':
          position.horizontal += instruction.value;
          break;
        case 'L':
          direction = this._getNewDirection(direction, instruction.value, 'left');
          break;
        case 'R':
          direction = this._getNewDirection(direction, instruction.value, 'right');
          break;
        case 'F':
          switch (direction) {
            case Direction.North:
              position.vertical -= instruction.value;
              break;
            case Direction.East:
              position.horizontal += instruction.value;
              break;
            case Direction.South:
              position.vertical += instruction.value;
              break;
            case Direction.West:
              position.horizontal -= instruction.value;
              break;
          }
          break;
      }
    }

    return position;
  }

  private _moveShipToWaypoint(instructions: Instruction[]): Position {
    const shipPos = new Position(0, 0);
    let waypointPos = new Position(10, -1);

    for (const instruction of instructions) {
      switch (instruction.action) {
        case 'N':
          waypointPos.vertical -= instruction.value;
          break;
        case 'W':
          waypointPos.horizontal -= instruction.value;
          break;
        case 'S':
          waypointPos.vertical += instruction.value;
          break;
        case 'E':
          waypointPos.horizontal += instruction.value;
          break;
        case 'L':
          waypointPos = this._rotateWaypoint(waypointPos, instruction.value, 'left');
          break;
        case 'R':
          waypointPos = this._rotateWaypoint(waypointPos, instruction.value, 'right');
          break;
        case 'F':
          shipPos.vertical += waypointPos.vertical * instruction.value;
          shipPos.horizontal += waypointPos.horizontal * instruction.value;
          break;
      }
    }

    return shipPos;
  }

  private _addManhattanPosition(number1: number, number2: number): number {
    return Math.abs(number1) + Math.abs(number2);
  }

  private _getNewDirection(start: number, degrees: number, leftOrRight: string): Direction {
    const steps = (degrees / 90) % 4;
    let result = 0;

    if (leftOrRight === 'left') {
      result = start - steps;
      if (result < 0) {
        result = 4 - -result;
      }
    }

    if (leftOrRight === 'right') {
      result = start + steps;
      if (result > 3) {
        result = -4 + result;
      }
    }

    return result as Direction;
  }

  private _rotateWaypoint(waypointPos: Position, degrees: number, leftOrRight: string): Position {
    const newPosition = new Position(0, 0);
    const oldDirections: Direction[] = [
      waypointPos.horizontal > 0 ? Direction.East : Direction.West,
      waypointPos.vertical > 0 ? Direction.South : Direction.North
    ];

    const newDirections: Direction[] = [
      this._getNewDirection(oldDirections[0], degrees, leftOrRight),
      this._getNewDirection(oldDirections[1], degrees, leftOrRight)
    ];

    for (let i = 0; i < newDirections.length; i++) {
      switch (newDirections[i]) {
        case Direction.North:
          newPosition.vertical = this._isVerticalDirection(oldDirections[i])
            ? -Math.abs(waypointPos.vertical)
            : -Math.abs(waypointPos.horizontal);
          break;
        case Direction.East:
          newPosition.horizontal = this._isVerticalDirection(oldDirections[i])
            ? Math.abs(waypointPos.vertical)
            : Math.abs(waypointPos.horizontal);
          break;
        case Direction.South:
          newPosition.vertical = this._isVerticalDirection(oldDirections[i])
            ? Math.abs(waypointPos.vertical)
            : Math.abs(waypointPos.horizontal);
          break;
        case Direction.West:
          newPosition.horizontal = this._isVerticalDirection(oldDirections[i])
            ? -Math.abs(waypointPos.vertical)
            : -Math.abs(waypointPos.horizontal);
          break;
      }
    }

    return newPosition;
  }

  private _isVerticalDirection(direction: Direction): boolean {
    return direction % 2 === 0;
  }
}