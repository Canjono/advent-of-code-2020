import * as fs from 'fs';

enum PolicyRule {
  Amount,
  Position
}

class PasswordPolicy {
  password = '';
  letter = '';
  firstNumber = 0;
  secondNumber = 0;
}

export class Day2 {
  run(): void {
    console.log('Day 2');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const passwordPolicies = this._getPasswordPolicies();
    const matches = this._getPolicyRuleMatches(passwordPolicies, PolicyRule.Amount);

    console.log('Solution 1:', matches.length);
  }

  private _solution2(): void {
    const passwordPolicies = this._getPasswordPolicies();
    const matches = this._getPolicyRuleMatches(passwordPolicies, PolicyRule.Position);

    console.log('Solution 2:', matches.length);
  }

  private _getPasswordPolicies(): PasswordPolicy[] {
    const regex = /^(\d+)-(\d+) (\w): (\w+)$/;

    const passwordPolicies = fs.readFileSync('src/day02/input.txt', 'utf-8')
      .split('\n')
      .map((line: string) => {
        const matches = line.match(regex);

        if (!matches) {
          throw new Error(`Line '${line}' did not match the password and policy pattern`);
        }

        const passwordPolicy = new PasswordPolicy();
        passwordPolicy.firstNumber = parseInt(matches[1], 10);
        passwordPolicy.secondNumber = parseInt(matches[2], 10);
        passwordPolicy.letter = matches[3];
        passwordPolicy.password = matches[4];

        return passwordPolicy;
      });

    return passwordPolicies;
  }

  private _getPolicyRuleMatches(passwordPolicies: PasswordPolicy[], rule: PolicyRule): PasswordPolicy[] {
    let matches: PasswordPolicy[];

    switch (rule) {
      case PolicyRule.Amount:
        matches = passwordPolicies.filter(this._matchesAmountRule);
        break;
      case PolicyRule.Position:
        matches = passwordPolicies.filter(this._matchesPositionRule);
        break;
      default:
        matches = [];
    }

    return matches;
  }

  private _matchesAmountRule(passwordPolicy: PasswordPolicy): boolean {
    let matchingAmount = 0;

    for (const letter of passwordPolicy.password) {
      if (letter === passwordPolicy.letter) {
        matchingAmount++;
      }
    }

    return matchingAmount >= passwordPolicy.firstNumber &&
      matchingAmount <= passwordPolicy.secondNumber;
  }

  private _matchesPositionRule(passwordPolicy: PasswordPolicy): boolean {
    const pos1 = passwordPolicy.firstNumber - 1;
    const pos2 = passwordPolicy.secondNumber - 1;
    const password = passwordPolicy.password;

    const firstPosMatches = password[pos1] === passwordPolicy.letter;
    const secondPosMatches = password[pos2] === passwordPolicy.letter;

    const exactlyOneMatches = firstPosMatches && !secondPosMatches ||
      !firstPosMatches && secondPosMatches;

    return exactlyOneMatches;
  }
}
