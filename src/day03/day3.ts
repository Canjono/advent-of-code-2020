import * as fs from 'fs';

interface ISlope {
  right: number;
  down: number;
}

class Map {
  private _map: string[];
  width: number;
  height: number;

  constructor(lines: string[]) {
    this.width = lines[0].length;
    this.height = lines.length;
    this._map = lines;
  }

  getLocationContent(y: number, x: number): string {
    return this._map[y][x];
  }
}

export class Day3 {
  private _tree = '#';

  run(): void {
    console.log('Day 3');

    this._solution1();
    this._solution2();
  }

  private _solution1(): void {
    const map = this._getMap();
    const slope = { right: 3, down: 1 };
    const hitTrees = this._calculateHitTrees(map, slope);

    console.log('Solution 1:', hitTrees);
  }

  private _solution2(): void {
    const map = this._getMap();
    const slopes = [
      { right: 1, down: 1 },
      { right: 3, down: 1 },
      { right: 5, down: 1 },
      { right: 7, down: 1 },
      { right: 1, down: 2 }
    ];
    const hitTrees = slopes.map(slope => this._calculateHitTrees(map, slope));
    const answer = this._multiplyHitTrees(hitTrees);

    console.log('Solution 2:', answer);
  }

  private _getMap(): Map {
    const lines = fs.readFileSync('src/day03/input.txt', 'utf-8')
      .split('\n');

    return new Map(lines);
  }

  private _calculateHitTrees(map: Map, slope: ISlope): number {
    let posX = 0;
    let hitTrees = 0;

    for (let posY = 0; posY < map.height; posY += slope.down) {
      if (map.getLocationContent(posY, posX) === this._tree) {
        hitTrees++;
      }
      posX = (posX + slope.right) % map.width;
    }

    return hitTrees;
  }

  private _multiplyHitTrees(hitTrees: number[]): number {
    return hitTrees.reduce((acc, curr) => acc * curr);
  }
}
